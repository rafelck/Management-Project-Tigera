@extends('layouts.app')

@section('content')
    <h3 class="page-title">@lang('quickadmin.client-currencies.title')</h3>
    
    {!! Form::model($client_currency, ['method' => 'PUT', 'route' => ['admin.client_currencies.update', $client_currency->id]]) !!}

    <div class="panel panel-default">
        <div class="panel-heading">
            @lang('quickadmin.qa_edit')
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('title', trans('quickadmin.client-currencies.fields.title'), ['class' => 'control-label']) !!}
                    {!! Form::text('title', old('title'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('title'))
                        <p class="help-block">
                            {{ $errors->first('title') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('code', trans('quickadmin.client-currencies.fields.code'), ['class' => 'control-label']) !!}
                    {!! Form::text('code', old('code'), ['class' => 'form-control', 'placeholder' => '', 'required' => '']) !!}
                    <p class="help-block"></p>
                    @if($errors->has('code'))
                        <p class="help-block">
                            {{ $errors->first('code') }}
                        </p>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12 form-group">
                    {!! Form::label('main_currency', trans('quickadmin.client-currencies.fields.main-currency'), ['class' => 'control-label']) !!}
                    {!! Form::hidden('main_currency', 0) !!}
                    {!! Form::checkbox('main_currency', 1, old('main_currency'), []) !!}
                    <p class="help-block"></p>
                    @if($errors->has('main_currency'))
                        <p class="help-block">
                            {{ $errors->first('main_currency') }}
                        </p>
                    @endif
                </div>
            </div>
            
        </div>
    </div>

    {!! Form::submit(trans('quickadmin.qa_update'), ['class' => 'btn btn-danger']) !!}
    {!! Form::close() !!}
@stop

