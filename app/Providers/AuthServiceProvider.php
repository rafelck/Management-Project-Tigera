<?php

namespace App\Providers;

use App\Role;
use App\User;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $user = \Auth::user();

        
        // Auth gates for: User management
        Gate::define('user_management_access', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Roles
        Gate::define('role_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('role_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Users
        Gate::define('user_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('user_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Contact management
        Gate::define('contact_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Contact companies
        Gate::define('contact_company_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_company_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Contacts
        Gate::define('contact_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('contact_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Time management
        Gate::define('time_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Time work types
        Gate::define('time_work_type_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_work_type_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_work_type_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_work_type_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_work_type_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Time projects
        Gate::define('time_project_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_project_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_project_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_project_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_project_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Time entries
        Gate::define('time_entry_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_entry_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_entry_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_entry_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('time_entry_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Time reports
        Gate::define('time_report_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Task management
        Gate::define('task_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Task statuses
        Gate::define('task_status_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_status_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_status_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_status_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_status_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Task tags
        Gate::define('task_tag_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_tag_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_tag_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_tag_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_tag_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Tasks
        Gate::define('task_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('task_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Task calendar
        Gate::define('task_calendar_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Internal notifications
        Gate::define('internal_notification_access', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('internal_notification_create', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('internal_notification_edit', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('internal_notification_view', function ($user) {
            return in_array($user->role_id, [1]);
        });
        Gate::define('internal_notification_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client management
        Gate::define('client_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Client management settings
        Gate::define('client_management_setting_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Client currencies
        Gate::define('client_currency_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_currency_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_currency_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_currency_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_currency_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client transaction types
        Gate::define('client_transaction_type_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_type_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_type_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_type_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_type_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client income sources
        Gate::define('client_income_source_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_income_source_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_income_source_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_income_source_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_income_source_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client statuses
        Gate::define('client_status_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_status_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_status_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_status_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_status_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client project statuses
        Gate::define('client_project_status_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_status_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_status_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_status_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_status_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Clients
        Gate::define('client_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client projects
        Gate::define('client_project_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_project_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client notes
        Gate::define('client_note_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_note_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_note_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_note_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_note_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client documents
        Gate::define('client_document_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_document_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_document_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_document_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_document_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client transactions
        Gate::define('client_transaction_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('client_transaction_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Client reports
        Gate::define('client_report_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Content management
        Gate::define('content_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Content categories
        Gate::define('content_category_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_category_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_category_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_category_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_category_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Content tags
        Gate::define('content_tag_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_tag_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_tag_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_tag_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_tag_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Content pages
        Gate::define('content_page_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_page_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_page_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_page_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('content_page_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Expense management
        Gate::define('expense_management_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

        // Auth gates for: Expense category
        Gate::define('expense_category_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_category_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_category_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_category_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_category_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Income category
        Gate::define('income_category_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_category_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_category_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_category_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_category_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Income
        Gate::define('income_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('income_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Expense
        Gate::define('expense_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_create', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_edit', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_view', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });
        Gate::define('expense_delete', function ($user) {
            return in_array($user->role_id, [1]);
        });

        // Auth gates for: Monthly report
        Gate::define('monthly_report_access', function ($user) {
            return in_array($user->role_id, [1, 2]);
        });

    }
}
